import { CurrencyProvider } from "src/app/providers/currency/currency";

/**
 * Environment: 'development'
 */
export const env = {
    name: 'development',
    enableAnimations: true,
    ratesAPI: new CurrencyProvider().getRatesApi(),
    activateScanner: true,
    // Production
    // awsUrl: 'https://aws.abcpay.cash/bws/api' 
    // Staging
    awsUrl: 'https://aws-staging.abcpay.cash/bws/api'
};
export default env;